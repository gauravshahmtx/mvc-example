import React, { Component } from 'react';
import loginController from '../controllers/loginController'
import '../App.css';
import './login.css';

export default class LoginView extends Component {

  onLoginClick() {
    let username = document.querySelector('input[name="username"').value;
    let password = document.querySelector('input[name="password"').value;
    const requestData = {
      username, password
    }
    loginController.login(requestData).then(result => {
      //navigate to next page
      window.alert("logged in")
    }).catch(error => {

    });
  }

  render() {
    return <div className="App">
      <header className="App-header">
        <div className="container">
          <input name="username" placeholder="username"></input>
          <input name="password" type="password" placeholder="password"></input>
          <button onClick={this.onLoginClick}>Submit</button>
        </div>
      </header>
    </div>;
  }
}
